if myHero.charName ~= "Nami" then
  return
end
require("VPrediction")
require("SOW")
if VIP_USER then
  require("Prodiction")
end
function OnLoad()
  NamiMenu()
  MenuLoaded = false
  PrintChat("<font color='#0066FF'> >> Nami - The Little Grown Mermaid<<</font>")
end
  Spells = {
    Q = {
      key = _Q,
      name = "Aqua Prison",
      range = 875,
      ready = false,
      dmg = 0,
      data = myHero:GetSpellData(_Q),
      speed = 1750,
      delay = 0.5,
      width = 80,
      pos = nil
    },
    W = {
      key = _W,
      name = "Ebb and Flow",
      range = 725,
      ready = false,
      dmg = 0,
      data = myHero:GetSpellData(_W)
    },
    E = {
      key = _E,
      name = "Tidecaller's Blessing",
      range = 800,
      ready = false,
      dmg = 0,
      data = myHero:GetSpellData(_E)
    },
    R = {
      key = _R,
      name = "Tidal Wave",
      range = 2550,
      ready = false,
      dmg = 0,
      data = myHero:GetSpellData(_R),
      speed = 1200,
      delay = 0.25,
      width = 325,
      pos = nil
    }
  }
  vPred = VPrediction()
  nSOW = SOW(vPred)
  TargetSelector = TargetSelector(TARGET_LESS_CAST_PRIORITY, Spells.Q.range, DAMAGE_MAGIC)
  TargetSelector.name = "Nami"
  Buffs = {
    BUFF_STUN,
    BUFF_ROOT,
    BUFF_KNOCKUP,
    BUFF_SUPPRESS,
    BUFF_SLOW,
    BUFF_CHARM,
    BUFF_FEAR,
    BUFF_TAUNT
  }
  InterruptingSpells = {
    AbsoluteZero = true,
    AlZaharNetherGrasp = true,
    CaitlynAceintheHole = true,
    Crowstorm = true,
    DrainChannel = true,
    FallenOne = true,
    GalioIdolOfDurand = true,
    InfiniteDuress = true,
    KatarinaR = true,
    MissFortuneBulletTime = true,
    Teleport = true,
    Pantheon_GrandSkyfall_Jump = true,
    ShenStandUnited = true,
    UrgotSwap2 = true
  }
  priorityTable = {
    AP = {
      "Annie",
      "Ahri",
      "Akali",
      "Anivia",
      "Annie",
      "Brand",
      "Cassiopeia",
      "Diana",
      "Evelynn",
      "FiddleSticks",
      "Fizz",
      "Gragas",
      "Heimerdinger",
      "Karthus",
      "Kassadin",
      "Katarina",
      "Kayle",
      "Kennen",
      "Morgana",
      "Lissandra",
      "Lux",
      "Malzahar",
      "Mordekaiser",
      "Morgana",
      "Nidalee",
      "Orianna",
      "Ryze",
      "Sion",
      "Swain",
      "Syndra",
      "Teemo",
      "TwistedFate",
      "Veigar",
      "Viktor",
      "Vladimir",
      "Xerath",
      "Ziggs",
      "Zyra"
    },
    Support = {
      "Alistar",
      "Blitzcrank",
      "Braum",
      "Janna",
      "Karma",
      "Leona",
      "Lulu",
      "Nami",
      "Nunu",
      "Sona",
      "Soraka",
      "Taric",
      "Thresh",
      "Zilean"
    },
    Tank = {
      "Amumu",
      "Chogath",
      "DrMundo",
      "Galio",
      "Hecarim",
      "Malphite",
      "Maokai",
      "Nasus",
      "Rammus",
      "Sejuani",
      "Nautilus",
      "Shen",
      "Singed",
      "Skarner",
      "Volibear",
      "Warwick",
      "Yorick",
      "Zac"
    },
    AD_Carry = {
      "Ashe",
      "Caitlyn",
      "Corki",
      "Draven",
      "Ezreal",
      "Graves",
      "Jayce",
      "Jinx",
      "KogMaw",
      "Lucian",
      "MasterYi",
      "MissFortune",
      "Pantheon",
      "Quinn",
      "Shaco",
      "Sivir",
      "Talon",
      "Tryndamere",
      "Tristana",
      "Twitch",
      "Urgot",
      "Varus",
      "Vayne",
      "Yasuo",
      "Zed"
    },
    Bruiser = {
      "Aatrox",
      "Darius",
      "Elise",
      "Fiora",
      "Gangplank",
      "Garen",
      "Irelia",
      "JarvanIV",
      "Jax",
      "Khazix",
      "LeeSin",
      "Nocturne",
      "Olaf",
      "Poppy",
      "Renekton",
      "Rengar",
      "Riven",
      "Rumble",
      "Shyvana",
      "Trundle",
      "Udyr",
      "Vi",
      "MonkeyKing",
      "XinZhao"
    }
  }
  Items = {
    BLACKFIRE = {
      id = 3188,
      range = 750,
      ready = false,
      dmg = 0
    },
    BRK = {
      id = 3153,
      range = 500,
      ready = false,
      dmg = 0
    },
    BWC = {
      id = 3144,
      range = 450,
      ready = false,
      dmg = 0
    },
    DFG = {
      id = 3128,
      range = 750,
      ready = false,
      dmg = 0
    },
    HXG = {
      id = 3146,
      range = 700,
      ready = false,
      dmg = 0
    },
    ODYNVEIL = {
      id = 3180,
      range = 525,
      ready = false,
      dmg = 0
    },
    DVN = {
      id = 3131,
      range = 200,
      ready = false,
      dmg = 0
    },
    ENT = {
      id = 3184,
      range = 350,
      ready = false,
      dmg = 0
    },
    HYDRA = {
      id = 3074,
      range = 350,
      ready = false,
      dmg = 0
    },
    TIAMAT = {
      id = 3077,
      range = 350,
      ready = false,
      dmg = 0
    },
    YGB = {
      id = 3142,
      range = 350,
      ready = false,
      dmg = 0
    }
  }
  local gameState = GetGame()
  if gameState.map.shortName == "twistedTreeline" then
    TTMAP = true
  else
    TTMAP = false
  end
  if heroManager.iCount < 10 then
    PrintChat(" >> Too few champions to arrange priority")
  elseif heroManager.iCount == 6 and TTMAP then
    ArrangeTTPrioritys()
  else
    ArrangePrioritys()
  end
function ArrangePrioritys()
  for i, enemy in pairs(GetEnemyHeroes()) do
    SetPriority(priorityTable.AD_Carry, enemy, 1)
    SetPriority(priorityTable.AP, enemy, 2)
    SetPriority(priorityTable.Support, enemy, 3)
    SetPriority(priorityTable.Bruiser, enemy, 4)
    SetPriority(priorityTable.Tank, enemy, 5)
  end
end
function ArrangeTTPrioritys()
  for i, enemy in pairs(GetEnemyHeroes()) do
    SetPriority(priorityTable.AD_Carry, enemy, 1)
    SetPriority(priorityTable.AP, enemy, 1)
    SetPriority(priorityTable.Support, enemy, 2)
    SetPriority(priorityTable.Bruiser, enemy, 2)
    SetPriority(priorityTable.Tank, enemy, 3)
  end
end
function SetPriority(table, hero, priority)
  for i = 1, #table do
    if hero.charName:find(table[i]) ~= nil then
      TS_SetHeroPriority(priority, hero.charName)
    end
  end
end
function NamiMenu()
  NamiMenu = scriptConfig("Nami - The Little Grown Mermaid ", "Nami")
  NamiMenu:addSubMenu("[Nintendo " .. myHero.charName .. "] - Skills Settings", "skills")
  NamiMenu.skills:addSubMenu("" .. Spells.Q.name .. " (Q)", "q")
  NamiMenu.skills.q:addParam("chainCC", "Auto Q Chain CC", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills.q:addParam("autoQ", "Auto Q", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills.q:addParam("interrupt", "Interrupt Spells with Q", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills.q:addParam("minQ", "Min # of Enemies", SCRIPT_PARAM_LIST, 2, {
    "1 Enemy",
    "2 Enemies",
    "3 Enemies",
    "4 Enemies",
    "5 Enemies"
  })
  NamiMenu.skills.q:addParam("qMana", "Min Mana % for Auto Q", SCRIPT_PARAM_SLICE, 40, 0, 100, -1)
  NamiMenu.skills.q:addParam("predType", "Prediction Use", SCRIPT_PARAM_LIST, 2, {
    "Prodiction",
    "VPrediction"
  })
  NamiMenu.skills.q:addParam("center", "Try to hit in Center", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills:addSubMenu("" .. Spells.W.name .. " (W)", "w")
  NamiMenu.skills.w:addParam("packetCast", "Cast with Packets", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills.w:addParam("autoW", "Auto Heal Allies", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills.w:addParam("minHealth", "Heal Allies if Health < %", SCRIPT_PARAM_SLICE, 75, 0, 100, -1)
  NamiMenu.skills.w:addParam("minMana", "Heal Allies if Mana > %", SCRIPT_PARAM_SLICE, 60, 0, 100, -1)
  NamiMenu.skills:addSubMenu("" .. Spells.E.name .. " (E)", "e")
  NamiMenu.skills.e:addParam("packetCast", "Cast with Packets", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills.e:addParam("autoE", "Auto E Allies", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.skills.e:addParam("eSelf", "Dont Use E on Myself", SCRIPT_PARAM_ONOFF, false)
  NamiMenu.skills.e:addParam("minMana", "Auto E Allies if Mana > %", SCRIPT_PARAM_SLICE, 50, 0, 100, -1)
  NamiMenu.skills:addSubMenu("" .. Spells.R.name .. " (R)", "r")
  NamiMenu.skills.r:addParam("manualR", "Manual Ult Key (T)", SCRIPT_PARAM_ONKEYDOWN, false, 84)
  NamiMenu.skills.r:addParam("autoR", "Auto R", SCRIPT_PARAM_ONOFF, false)
  NamiMenu.skills.r:addParam("minR", "Min # of Enemies", SCRIPT_PARAM_LIST, 2, {
    "1 Enemy",
    "2 Enemies",
    "3 Enemies",
    "4 Enemies",
    "5 Enemies"
  })
  NamiMenu.skills.r:addParam("predType", "Prediction Use", SCRIPT_PARAM_LIST, 2, {
    "Prodiction",
    "VPrediction"
  })
  NamiMenu.skills:addSubMenu("Ignite", "ignite")
  NamiMenu.skills.ignite:addParam("auto", "Use Auto Ignite", SCRIPT_PARAM_ONOFF, true)
  NamiMenu:addSubMenu("[Nintendo " .. myHero.charName .. "] - Combo Settings", "combo")
  NamiMenu.combo:addParam("comboKey", "Combo Key (X)", SCRIPT_PARAM_ONKEYDOWN, false, 88)
  NamiMenu.combo:addParam("comboE", "Use " .. Spells.E.name .. " (E) in Combo", SCRIPT_PARAM_ONOFF, false)
  NamiMenu.combo:addParam("comboItems", "Use Items With Combo", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.combo:permaShow("comboKey")
  NamiMenu:addSubMenu("[Nintendo " .. myHero.charName .. "] - Harass Settings", "harass")
  NamiMenu.harass:addParam("harassKey", "Harass Hotkey (C)", SCRIPT_PARAM_ONKEYDOWN, false, 67)
  NamiMenu.harass:addParam("Q", "Use " .. Spells.Q.name .. " (Q)", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.harass:addParam("W", "Use " .. Spells.W.name .. " (W)", SCRIPT_PARAM_ONOFF, false)
  NamiMenu.harass:addParam("E", "Use " .. Spells.E.name .. " (E)", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.harass:permaShow("harassKey")
  NamiMenu:addSubMenu("[Nintendo " .. myHero.charName .. "] - Orbwalking Settings", "Orbwalking")
  nSOW:LoadToMenu(NamiMenu.Orbwalking)
  NamiMenu:addSubMenu("[Nintendo " .. myHero.charName .. "] - Drawing Settings", "drawing")
  NamiMenu.drawing:addParam("qDraw", "Draw " .. Spells.Q.name .. " (Q) Range", SCRIPT_PARAM_ONOFF, true)
  NamiMenu.drawing:addParam("wDraw", "Draw " .. Spells.W.name .. " (W) Range", SCRIPT_PARAM_ONOFF, false)
  NamiMenu.drawing:addParam("eDraw", "Draw " .. Spells.E.name .. " (E) Range", SCRIPT_PARAM_ONOFF, false)
  NamiMenu.drawing:addParam("counter", "Q Landed Counter", SCRIPT_PARAM_ONOFF, true)
  NamiMenu:addTS(TargetSelector)
  MenuLoaded = true
end
function OnTick()
  if not MenuLoaded then
    return
  end
  Checks()
  AutoSkills()
  ComboKey = NamiMenu.combo.comboKey
  harassKey = NamiMenu.harass.harassKey
  if ComboKey then
    NamiCombo()
  end
  if harassKey then
    HarassCombo()
  end
  if NamiMenu.skills.ignite.auto then
    AutoIgnite()
  end
  if NamiMenu.skills.q.center then
    Spells.Q.width = 80
    Spells.Q.delay = 0.8
  else
    Spells.Q.width = 180
    Spells.Q.delay = 0.8
  end
end
function NamiCombo()
  if Target and Target.valid then
    CastQ(Target)
    CastW()
    if NamiMenu.combo.comboE then
      CastE()
    end
  end
end
function HarassCombo()
  if Target and Target.valid then
    if NamiMenu.harass.E then
      CastE()
    end
    if NamiMenu.harass.Q then
      CastQ(Target)
    end
    if NamiMenu.harass.W then
      CastW()
    end
  end
end
function AutoSkills()
  for _, enemy in pairs(GetEnemyHeroes()) do
    if ValidTarget(enemy) and enemy.visible then
      if NamiMenu.skills.q.autoQ and Spells.Q.ready and GetDistanceSqr(enemy.visionPos) < Spells.Q.range * Spells.Q.range and myHero.mana >= myHero.maxMana * (NamiMenu.skills.q.qMana / 100) then
        if VIP_USER and NamiMenu.skills.q.predType == 1 and Prodiction.IsDonator() then
          local bool, pos, info = Prodiction.GetMinCountCircularAOEPrediction(NamiMenu.skills.q.minQ, Spells.Q.range, Spells.Q.speed, Spells.Q.delay, Spells.Q.width)
          if bool and pos and info.hitchance ~= 0 then
            CastSpell(_Q, pos.x, pos.z)
          end
        else
          local AOEPos, Chance, Targets = vPred:GetCircularAOECastPosition(enemy, Spells.Q.delay, Spells.Q.width, Spells.Q.range, Spells.Q.speed, myHero)
          if Chance >= 2 and Targets >= NamiMenu.skills.q.minQ then
            CastSpell(_Q, AOEPos.x, AOEPos.z)
          end
        end
      end
      if NamiMenu.skills.r.autoR or NamiMenu.skills.r.manualR and Spells.R.ready and GetDistanceSqr(enemy) < Spells.R.range * Spells.R.range then
        for _, ally in ipairs(GetAllies()) do
          local AlliesAround = CountAllyHeroInRange(1000, ally)
          local EnemiesAround = CountEnemyHeroInRange(1000, ally)
          if AlliesAround > EnemiesAround then
            if NamiMenu.skills.q.predType == 1 and Prodiction.IsDonator() then
              local bool, pos, info = Prodiction.GetMinCountLineAOEPrediction(NamiMenu.skills.r.minR, Spells.R.range, Spells.R.speed, Spells.R.delay, Spells.R.width)
              if bool and pos and info.hitchance ~= 0 then
                CastSpell(_R, pos.x, pos.z)
              end
            else
              local rAOEPos, rChance, rTargets = vPred:GetLineAOECastPosition(enemy, Spells.R.delay, Spells.R.width, Spells.R.range, Spells.R.speed, myHero)
              if rChance >= 2 and rTargets >= NamiMenu.skills.r.minR then
                CastSpell(_R, rAOEPos.x, rAOEPos.z)
              end
            end
          end
        end
      end
    end
  end
  if NamiMenu.skills.w.autoW and Spells.W.ready then
    for _, ally in pairs(GetAllyHeroes()) do
      local MenuHealth = NamiMenu.skills.w.minHealth / 100
      local MenuMana = NamiMenu.skills.w.minMana / 100
      if ally.health <= ally.maxHealth * MenuHealth and myHero.mana >= myHero.maxMana * MenuMana then
        CastW()
      end
    end
  end
end
function CastW()
  local WTarget
  if Spells.W.ready then
    local MenuHealth = NamiMenu.skills.w.minHealth / 100
    for _, ally in pairs(GetAllies()) do
      if not ally.dead and GetDistanceSqr(ally) <= Spells.W.range * Spells.W.range and ally.health <= ally.maxHealth * MenuHealth and CountEnemyHeroInRange(Spells.W.range, ally) >= 1 then
        WTarget = ally
      end
    end
    if WTarget == nil then
      for _, enemy in pairs(GetEnemyHeroes()) do
        if not enemy.dead and enemy.visible and GetDistanceSqr(enemy) <= Spells.W.range * Spells.W.range and (1 <= CountAllyHeroInRange(Spells.W.range, enemy) or CountEnemyHeroInRange(Spells.W.range, enemy) >= 1) then
          WTarget = enemy
        end
      end
    end
    if Target and GetDistanceSqr(Target) <= Spells.W.range * Spells.W.range and WTarget == nil then
      WTarget = Target
    end
  end
  if WTarget ~= nil then
    if GetDistanceSqr(WTarget) > Spells.W.range * Spells.W.range or not Spells.W.ready then
      return false
    elseif VIP_USER and NamiMenu.skills.w.packetCast then
      Packet("S_CAST", {
        spellId = _W,
        targetNetworkId = WTarget.networkID
      }):send()
      return true
    else
      CastSpell(_W, WTarget)
      return true
    end
  end
end
function CastE()
  local ETarget
  if Spells.E.ready then
    for _, ally in ipairs(GetAllies()) do
      if GetDistanceSqr(ally) <= Spells.E.range * Spells.E.range and (ETarget == nil or ETarget.damage < ally.damage) then
        ETarget = ally
      end
      if ETarget == nil or myHero.damage > ETarget.damage and not NamiMenu.skills.e.eSelf then
        ETarget = myHero
      end
    end
  end
  if ETarget ~= nil then
    if GetDistanceSqr(ETarget) > Spells.E.range * Spells.E.range or not Spells.E.ready then
      return false
    elseif VIP_USER and NamiMenu.skills.e.packetCast then
      Packet("S_CAST", {
        spellId = _E,
        targetNetworkId = ETarget.networkID
      }):send()
      return true
    else
      CastSpell(_E, ETarget)
      return true
    end
  end
end
function CountAllyHeroInRange(range, object)
  object = object or myHero
  range = range * range
  local allyInRange = 0
  for i = 1, heroManager.iCount do
    local hero = heroManager:getHero(i)
    if hero.team == myHero.team and not hero.dead and range >= GetDistanceSqr(object, hero) then
      allyInRange = allyInRange + 1
    end
  end
  return allyInRange
end
function CastQ(unit)
  if GetDistanceSqr(unit.visionPos) > Spells.Q.range * Spells.Q.range or not Spells.Q.ready or not unit.valid then
    return false
  elseif VIP_USER then
    if NamiMenu.skills.q.predType == 1 then
      local pos, info = Prodiction.GetCircularAOEPrediction(unit.visionPos, Spells.Q.range, Spells.Q.speed, Spells.Q.delay, Spells.Q.width)
      if pos and 1 < info.hitchance then
        CastSpell(_Q, pos.x, pos.z)
        return true
      end
    else
      local CastPos, HitChance, Position = vPred:GetCircularCastPosition(unit.visionPos, Spells.Q.delay, Spells.Q.width, Spells.Q.speed)
      if HitChance >= 2 then
        CastSpell(_Q, CastPos.x, CastPos.z)
        return true
      end
    end
  else
    local QPrediction = TargetPrediction(Spells.Q.range, Spells.Q.speed, Spells.Q.delay, Spells.Q.width)
    Spells.Q.pos = QPrediction:GetPrediction(unit.visionPos)
    if Spells.Q.pos and Spells.Q.pos ~= nil then
      CastSpell(_Q, Spells.Q.pos.x, Spells.Q.pos.z)
      return true
    end
  end
  return false
end
function AutoIgnite()
  if ignitReady then
    for _, enemy in pairs(GetEnemyHeroes()) do
      if GetDistanceSqr(enemy) < 360000 then
        local igniteDmg = getDmg("IGNITE", enemy, myHero)
        if igniteDmg > enemy.health then
          CastSpell(ignit, enemy)
        end
      end
    end
  end
end
function GetTarget()
  TargetSelector:update()
  if _G.MMA_Target and _G.MMA_Target.type == myHero.type then
    return _G.MMA_Target
  elseif _G.AutoCarry and _G.AutoCarry.Crosshair and _G.AutoCarry.Attack_Crosshair then
    return _G.AutoCarry.Attack_Crosshair.target
  elseif TargetSelector.target and not TargetSelector.target.dead and TargetSelector.target.type == myHero.type then
    return TargetSelector.target
  else
    return nil
  end
end
function UseItems(enemy)
  for i, item in pairs(Items) do
    if GetInventoryItemIsCastable(item.id) and GetDistanceSqr(enemy) < item.range * item.range then
      CastItem(item.id, enemy)
    end
  end
end
function MoveToMouse()
  if GetDistance(mousePos) then
    local moveToPos = myHero + Vector(mousePos) - myHero:normalized() * 300
    myHero:MoveTo(moveToPos.x, moveToPos.z)
  end
end
function GetAllies()
  if _allyHeroes then
    return _allyHeroes
  end
  _allyHeroes = {}
  for i = 1, heroManager.iCount do
    local hero = heroManager:GetHero(i)
    if hero.team == player.team then
      table.insert(_allyHeroes, hero)
    end
  end
  return setmetatable(_allyHeroes, {
    __newindex = function(self, key, value)
      error("Adding to AllyHeroes is not granted. Use table.copy.")
    end
  })
end
function CountAllyHeroInRange(range, object)
  object = object or myHero
  range = range and range * range or myHero.range * myHero.range
  local enemyInRange = 0
  for i = 1, heroManager.iCount do
    local hero = heroManager:getHero(i)
    if hero.team == myHero.team and not hero.dead and hero.valid and range >= GetDistanceSqr(object, hero) then
      enemyInRange = enemyInRange + 1
    end
  end
  return enemyInRange
end
if VIP_USER then
  function OnGainBuff(source, buff)
    if not MenuLoaded then
      return
    end
    if NamiMenu.skills.q.chainCC and source.team ~= myHero.team and source.type == myHero.type then
      for i = 1, #Buffs do
        local buffType = Buffs[i]
        if buff.type == buffType then
          CastQ(source)
        end
      end
    end
  end
end
function OnProcessSpell(unit, spell)
  if not MenuLoaded then
    return
  end
  if unit.team == myHero.team and unit.type == myHero.type and NamiMenu.skills.e.autoE and spell.name:lower():find("attack") and GetDistanceSqr(unit) <= Spells.E.range * Spells.E.range then
    local MenuMana = NamiMenu.skills.e.minMana / 100
    if myHero.mana >= myHero.maxMana * MenuMana and spell.target.type == myHero.type then
      if unit ~= myHero then
        CastSpell(_E, unit)
      elseif unit == myHero and not NamiMenu.skills.e.eSelf then
        CastSpell(_E, unit)
      end
    end
  end
  if NamiMenu.skills.q.interrupt and GetDistanceSqr(unit) <= Spells.Q.range * Spells.Q.range and InterruptingSpells[spell.name] then
    CastSpell(_Q, unit.visionPos.x, unit.visionPos.z)
  end
end
function OnDraw()
  if not MenuLoaded then
    return
  end
  if not myHero.dead then
    if Spells.Q.ready and NamiMenu.drawing.qDraw then
      DrawCircle(myHero.x, myHero.y, myHero.z, Spells.Q.range, 39423)
    end
    if Spells.W.ready and NamiMenu.drawing.wDraw then
      DrawCircle(myHero.x, myHero.y, myHero.z, Spells.W.range, 3394611)
    end
    if Spells.E.ready and NamiMenu.drawing.eDraw then
      DrawCircle(myHero.x, myHero.y, myHero.z, Spells.E.range, 6724095)
    end
  end
end
function Checks()
  Target = GetTarget()
  for i, item in pairs(Items) do
    item.ready = GetInventoryItemIsCastable(item.id)
  end
  for i, spell in pairs(Spells) do
    spell.ready = myHero:CanUseSpell(spell.key) == READY
  end
  if myHero:GetSpellData(SUMMONER_1).name:find("summonerdot") then
    ignit = SUMMONER_1
  elseif myHero:GetSpellData(SUMMONER_2).name:find("summonerdot") then
    ignit = SUMMONER_2
  end
  ignitReady = ignit ~= nil and myHero:CanUseSpell(ignit) == READY
end
